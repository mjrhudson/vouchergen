# Libraries
import pathlib
import genvouchlib

# Paths
settingsPath = str(pathlib.Path(__file__).parent.absolute()) + "/" + "settings.json"
matchstrings, matchsalt, voucherlength, uidlength, numvouchers, genmethod, closeAfterFinish, verbose, logProgress = genvouchlib.loadGenSettings(settingsPath)
for voucherset in range(len(matchstrings)):
    genvouchlib.genvouchers(voucherset,matchstrings[voucherset],matchsalt,voucherlength,uidlength,numvouchers,genmethod,closeAfterFinish,verbose,logProgress)