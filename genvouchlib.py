# Libraries
import os
import platform
import random
import string
import zlib
import hashlib
import time
import math
import pathlib
import json
import sys

# Functions
def clearScreen():
    if platform.system()=="Windows":
        os.system('cls')
    elif (platform.system()=="Linux") or (platform.system()=="Darwin"):
        os.system('clear')

def crc32(strIn,salt):
    cv = 0
    cv = zlib.crc32(bytes(strIn,encoding="utf-8"),cv)
    cv = zlib.crc32(bytes(salt,encoding="utf-8"),cv)
    return (cv & 0xFFFFFFFF)

def genrandvoucher(voucherlength):
    allChars = string.ascii_lowercase + string.digits
    return ''.join(random.choice(allChars) for i in range(voucherlength))

def genuid(matchstring,matchsalt,uidlength,genmethod):
    if genmethod == "crc32":
        crc32out = str(crc32(matchstring,matchsalt))
        uid = crc32out[0:uidlength]
    elif genmethod == "sha1":
        prehash = matchstring + matchsalt
        hashout = hashlib.sha1(prehash.encode('utf-8')).hexdigest()
        hashoutsplit = [hashout[i:i+2] for i in range(0,len(hashout),2)]
        hashoutuint = [int(hashoutsplit[i],16) for i in range(len(hashoutsplit))]
        hashoutdiv = [0]*len(hashoutuint)
        for j in range(len(hashoutuint)):
            hashoutdiv[j] = int(round(hashoutuint[j]/31.875,0)+1)
        hashoutstr = ''.join(str(n) for n in hashoutdiv)
        uid = hashoutstr[0:uidlength]
    else:
        print(" Error: Invalid hash method chosen\n")
        input("Press enter to continue...")
        exit()
    return uid

def genvouchers(voucherset,matchstring,matchsalt,voucherlength,uidlength,numvouchers,genmethod,closeAfterFinish,verbose,logProgress):
    vouchersetstr = " (Voucher Set " + str(voucherset+1).zfill(2) + ")\n"
    genstr = " Generating " + str(numvouchers) + " vouchers using the " + genmethod.upper() + " method\n"
    matchstr = " Match String: " + matchstring
    saltstr = " Match Salt:   " + "".join(["*"]*len(matchsalt))
    if not verbose:
        clearScreen()
        print(vouchersetstr)
        print(" Generating vouchers...")
    if logProgress:
        try:
            fprog = open(str(pathlib.Path(__file__).parent.absolute()) + "/__temp__/" + str(voucherset+1).zfill(2) + ".prog","w")
            fprog.write("0")
            fprog.close()
        except:
            pass
    uid = genuid(matchstring,matchsalt,uidlength,genmethod)
    # Run
    tStart = time.perf_counter()
    uidstr = " Unique ID:    " + uid
    vouchers = [""]*numvouchers
    for i in range(numvouchers):
        if verbose:
            clearScreen()
            print(vouchersetstr)
            print(genstr)
            print(matchstr)
            print(saltstr)
            print(uidstr)
            print("")
            print(" Generating voucher " + str(i+1) + " of " + str(numvouchers) + "...")
        if logProgress:
            try:
                fprog = open(str(pathlib.Path(__file__).parent.absolute()) + "/__temp__/" + str(voucherset+1).zfill(2) + ".prog","w")
                fprog.write(str(i+1))
                fprog.close()
            except:
                pass
        while True:
            randvoucher = genrandvoucher(voucherlength)
            randuid = genuid(randvoucher,matchsalt,uidlength,genmethod)
            if uid == randuid and not randvoucher in vouchers:
                vouchers[i] = randvoucher
                break
    tEnd = time.perf_counter()
    tElapsed = int(round(tEnd-tStart,0))
    tHours = math.floor(tElapsed/3600)
    tMinutes = math.floor((tElapsed/60)-(tHours*60))
    tSeconds = math.floor(tElapsed-(tMinutes*60)-(tHours*3600))
    if tHours > 0:
        tStr = str(tHours).zfill(2) + " hours " + str(tMinutes).zfill(2) + " minutes " + str(tSeconds).zfill(2) + " seconds "
    elif tMinutes > 0:
        tStr = str(tMinutes).zfill(2) + " minutes " + str(tSeconds).zfill(2) + " seconds "
    else:
        tStr = str(tSeconds).zfill(2) + " seconds "
    tStrFull = " " + str(numvouchers) + " vouchers generated in " + tStr + "\n using the " + genmethod.upper() + " method\n"
    f = open(str(pathlib.Path(__file__).parent.absolute()) + "/output/" + str(voucherset+1).zfill(2) + "_vouchers.txt","w")
    f.write(matchstring + "\n")
    f.write(uid + "\n\n")
    f.write("Vouchers:\n")
    for i in range(len(vouchers)):
        if i != len(vouchers)-1:
            f.write(vouchers[i] + "\n")
        else:
            f.write(vouchers[i])
    f.close()
    if verbose:
        clearScreen()
        print(vouchersetstr)
        print(tStrFull)
        print(matchstr)
        print(saltstr)
        print(uidstr)
        print("")
        print(" Vouchers:\n")
        for i in range(len(vouchers)):
            print(" " + vouchers[i])
        print("\n --------------------------------------------------")
    else:
        clearScreen()
        print(vouchersetstr)
        print(" Finished")
    if logProgress:
        finishedLogging = False
        while not finishedLogging:
            try:
                fprog = open(str(pathlib.Path(__file__).parent.absolute()) + "/__temp__/" + str(voucherset+1).zfill(2) + ".prog","w")
                fprog.write("-1") # Indicate finished
                fprog.close()
                finishedLogging = True
            except:
                finishedLogging = False
            time.sleep(0.1)
    if not closeAfterFinish:
        input("\n Press enter to continue...")
        print("")
        exit()

def loadGenSettings(settingsPath):
    # Settings
    with open(settingsPath,"r") as f:
        settings = json.load(f)
    matchstrings = settings["matchstrings"]
    matchsalt = settings["matchsalt"]
    voucherlength = settings["voucherlength"]
    uidlength = settings["uidlength"]
    numvouchers = settings["numvouchers"]
    genmethod = settings["genmethod"].lower() # can be either "crc32" or "sha1"
    closeAfterFinish = settings["closeAfterFinish"]
    verbose = settings["verbose"]
    logProgress = settings["logProgress"]
    return matchstrings, matchsalt, voucherlength, uidlength, numvouchers, genmethod, closeAfterFinish, verbose, logProgress