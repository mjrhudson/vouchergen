# Voucher Generation

Voucher generation based on collisions of truncated cryptographic hashes.

I created this as a quick solution for a voucher-based system I needed to write for some other offline software I was working on at the time. It works by generating a hash of a known string (I chose the machine/computer name), truncating it and creating a unique ID from it. From here, random character combinations are hashed and truncated through the same method, with the goal of finding a hash 'collision' (whereby the random combination results in the same unique ID), which is then stored as a voucher for comparison and validation later.
